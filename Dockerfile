FROM siden/docker-bash:stable

LABEL maintainer="Siden <docker@siden.io>"

RUN apk add --update --no-cache curl openssl

# install kubectl
RUN curl -# -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl && \
    install kubectl /usr/local/bin/kubectl && rm -rf kubectl*

# install helm3
RUN curl -# https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

# install aws-iam-authenticator
RUN curl -# -LO https://amazon-eks.s3.us-west-2.amazonaws.com/1.17.9/2020-08-04/bin/linux/amd64/aws-iam-authenticator && \
    install aws-iam-authenticator /usr/local/bin/aws-iam-authenticator && rm -rf aws-iam-authenticator*

# install istiotctl
RUN export ISTIO_VERSION="$(curl -sL https://api.github.com/repos/istio/istio/releases/latest | jq -r '.tag_name')" && \
    curl -# -SL https://github.com/istio/istio/releases/download/${ISTIO_VERSION}/istio-${ISTIO_VERSION}-linux-amd64.tar.gz | \
    tar -xz -C /usr/local/bin --strip-components=2 istio-${ISTIO_VERSION}/bin/istioctl

# install tfenv
RUN git clone https://github.com/tfutils/tfenv.git ~/.tfenv && \
    ln -s ~/.tfenv/bin/* /usr/local/bin

# install latest version of terraform
RUN tfenv install latest && tfenv use latest

# install terragrunt
RUN curl -# -LO https://github.com/gruntwork-io/terragrunt/releases/latest/download/terragrunt_linux_amd64 && \
    install terragrunt_linux_amd64 /usr/local/bin/terragrunt && rm -rf terragrunt*

# install tflint
RUN curl -# -LO https://github.com/terraform-linters/tflint/releases/latest/download/tflint_linux_amd64.zip && \
    unzip tflint_linux_amd64.zip && \
    install tflint /usr/local/bin/tflint && rm -rf tflint*

# install kubectl tf provider
# https://github.com/gavinbunney/terraform-provider-kubectl#terraform-012
# alpine sh doesn't have pushd/popd so this uses `cd` instead
RUN mkdir -p ~/.terraform.d/plugins/terraform-provider-kubectl-tmp && \
    curl -Ls https://api.github.com/repos/gavinbunney/terraform-provider-kubectl/releases/latest \
    | jq -r ".assets[] | select(.browser_download_url | contains(\"$(uname -s | tr A-Z a-z)\")) | select(.browser_download_url | contains(\"amd64\")) | .browser_download_url" \
    | xargs -n 1 curl -Lo ~/.terraform.d/plugins/terraform-provider-kubectl.zip && \
    cd ~/.terraform.d/plugins/ && \
    unzip ~/.terraform.d/plugins/terraform-provider-kubectl.zip -d terraform-provider-kubectl-tmp && \
    mv terraform-provider-kubectl-tmp/terraform-provider-kubectl* . && \
    chmod +x terraform-provider-kubectl* && \
    rm -rf terraform-provider-kubectl-tmp && \
    rm -rf terraform-provider-kubectl.zip

ENTRYPOINT ["/usr/bin/env"]
